package com.santam.genex;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class test {
    @RequestMapping("/")
    public String home() {
        return "<h3>Genex Digital Docker and CI/CD Test<h3>";
    }
}
