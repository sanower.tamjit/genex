# genex-technical-test

### CI/CD & Docker
For CI/CD I'm using here gitlab.  
The CI/CD pipeline code [here gitlab-ci.yml](.gitlab-ci.yml)

Run the application using docker:
`docker run -p 8090:8080 registry.gitlab.com/sanower.tamjit/genex`

### Chatbot Development Platform Architecture
![Chatbot Development Platform Architecture](./images/PlatformArchitecture.png)  
  
  
### Tech Stack
- **Backend Languages:** Python(For ML and Data Engineering), Java(Platform Backend and Data Engineering, Javascript/Node.js (For communication services and )  
- **Messaging Services:** WebSocket, Apache Kafka (For Third Party Communications Pipeline),   
- **Databases:** RDBMS(Postgre), In-Memory Database(Redis), NoSQL Database(MongoDB)   
- **Cloud Services:** AWS, Elastic Load Balancing, Amazon Route 53, Amazon Cloudfront, etc.  
- **DevOps & Container:** Jenkins, Docker  
- **Web-Server and Security:** Apache Tomcat, Nginx, Cloudflare.  
- **Data-Analytics:** Google Data Studio, Google Analytics  
- **Frontend:** React.js(with Redux)


### Simple Database Design
![Simple Database Design](./images/Simple%20DatabaseDesign%20Chatbot%20Platform.png)  
\
\
\
**Personal Opinion:** To develop chatbot platform I will choose an open source platform like [rasa.io](https://rasa.io/) and modify it