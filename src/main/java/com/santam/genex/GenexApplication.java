package com.santam.genex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
public class GenexApplication {


    public static void main(String[] args) {
        SpringApplication.run(GenexApplication.class, args);
    }

}
